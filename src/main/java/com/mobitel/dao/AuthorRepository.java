package com.mobitel.dao;

import java.util.List;

import org.springframework.stereotype.Service;

import com.mobitel.entity.Author;

public interface AuthorRepository {

	public List<Author> findAuthors();

	public Author findById(Long Id);
	
	public String add(Author author);
	
	public String delete(Long id);
}
