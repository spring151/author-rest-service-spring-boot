package com.mobitel.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.mobitel.dao.AuthorRepository;
import com.mobitel.entity.Author;

@Service
public class AuthorRepositoryImpl implements AuthorRepository {

	@Override
	public List<Author> findAuthors() {
		return createDummyAuthors();
	}

	private List<Author> createDummyAuthors() {
		List<Author> authors = new ArrayList<>();

		authors.add(new Author(1l, "Sujith", "Delpachithra", "Male"));
		authors.add(new Author(2l, "Kishani", "Gunewardane", "Female"));
		return authors;
	}

	@Override
	public Author findById(Long Id) {
		List<Author> authors = createDummyAuthors();
		Optional<Author> author = authors.stream().filter(a -> a.getId().equals(Id)).findFirst();
		return author.orElse(null);
	}

	@Override
	public String add(Author author) {
		List<Author> authors = createDummyAuthors();
		authors.add(author);
		return "added";
	}

	@Override
	public String delete(Long id) {
		List<Author> authors = createDummyAuthors();
		authors.stream().dropWhile(a->a.getId().equals(id));
		return "deleted";
	}

}
