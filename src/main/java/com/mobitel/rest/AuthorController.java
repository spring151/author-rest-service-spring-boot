package com.mobitel.rest;

import java.util.ArrayList;
import java.util.List;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mobitel.entity.Author;
import com.mobitel.service.AuthorService;

@RestController
@RequestMapping("/api")
public class AuthorController {

	@Autowired
	private AuthorService authorService;

	@GetMapping("/authors")
	public ResponseEntity<List<Author>> getAuthors() {
		return ResponseEntity.ok(authorService.get());
	}

	@GetMapping("/authors/{id}")
	public ResponseEntity<Author> getAuthor(@PathVariable Long id) {

		try {
			Author author = authorService.get(id);
			return ResponseEntity.ok(author);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(null);
		}
	}
	
	@PostMapping("/authors")
	public ResponseEntity<String> saveAuthor(@RequestBody Author author) {

		return null;
	}
	
	@DeleteMapping("/authors/{id}")
	public ResponseEntity<String> deleteAuthor(@RequestBody Author author) {

		return null;
	}
}
