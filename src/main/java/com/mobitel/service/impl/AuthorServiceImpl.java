package com.mobitel.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobitel.dao.AuthorRepository;
import com.mobitel.entity.Author;
import com.mobitel.service.AuthorService;

@Service
public class AuthorServiceImpl implements AuthorService {


	@Autowired
	AuthorRepository authorRepository;
	
	@Override
	public List<Author> get() {
		return authorRepository.findAuthors();
	}

	@Override
	public Author get(Long Id) {
		Author author= authorRepository.findById(Id);
		
		if(author !=null) {
			return author;
		}
		throw new RuntimeException("No Author found.");
	}

	@Override
	public String put(Author author) {
		authorRepository.add(author);
		return "added";
	}

	@Override
	public String delete(Long id) {
		authorRepository.delete(id);
		return "deleted";
	}

}
