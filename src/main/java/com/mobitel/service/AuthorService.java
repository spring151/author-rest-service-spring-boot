package com.mobitel.service;

import java.util.List;

import com.mobitel.entity.Author;

public interface AuthorService {

	public List<Author> get();
	
	public Author get(Long Id);
	
	public String put(Author author);
	
	public String delete(Long id);
}
